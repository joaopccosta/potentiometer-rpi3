FROM resin/raspberrypi3-python

WORKDIR /usr/src/app

COPY ./dependencies.txt /dependencies.txt

RUN pip install -r /dependencies.txt

COPY . ./

ENV INITSYSTEM on

CMD ["python", "src/main.py"]
