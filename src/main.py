import RPi.GPIO as GPIO
import time
import serial

port =serial.Serial("/dev/ttyS0",baudrate=57600,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS,writeTimeout = 0,timeout = 10,rtscts=False,dsrdtr=False,xonxoff=False)

GPIO.setmode(GPIO.BCM)

potentiometer1pins = [18,23]
potentiometer2pins = [24,25]
running = True

def discharge(pins):
    GPIO.setup(pins[0], GPIO.IN)
    GPIO.setup(pins[1], GPIO.OUT)
    GPIO.output(pins[1], False)
    time.sleep(0.005)

def charge_time(pins):
    GPIO.setup(pins[1], GPIO.IN)
    GPIO.setup(pins[0], GPIO.OUT)
    count = 0
    GPIO.output(pins[0], True)
    while not GPIO.input(pins[1]):
        count = count + 1
    return count

def analog_read(pins):
    discharge(pins)
    return charge_time(pins)

try:
	while running:
		dataFromPotentiometerOne = analog_read(potentiometer1pins)
		dataFromPotentiometerTwo = "0" #analog_read(potentiometer2pins)			# there seems to be a limitation with gpio
		print(`dataFromPotentiometerOne`+';'+`dataFromPotentiometerTwo`+';')
		port.write(`dataFromPotentiometerOne`+';'+`dataFromPotentiometerTwo`+';')	
		time.sleep(1)
	print("Done")
	
except KeyboardInterrupt:  
	print("Forced stop")
	running = False
	
finally:  
    GPIO.cleanup()